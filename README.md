# ec2internaldns

Simple DNS resolver for use on-premises to resolve Amazon 
ip-a-b-c-d.ec2.internal requests into IP addresses.  Allows on-prem 
systems to use Amazon-generated names successfully.

This resolver is designed for use as a conditional forwarder from
your existing DNS infrastructure.  Specifically, an entry into Bind
might look like this:

```
zone "ec2.internal" IN {
    type forward;
    forwarders {
        10.20.30.40;
    };
};
```
