package main

import (
	"fmt"
	"github.com/miekg/dns"
	"log"
	"regexp"
	"strconv"
	"strings"
)

var ec2internalFormat *regexp.Regexp

func extractIp(s string) (string, bool) {
	octets := ec2internalFormat.FindAllStringSubmatch(s, -1)
	if len(octets) == 0 {
		return "", false
	}

	// Validation checks for just the 4 ip address octets
	for i := 1; i <= 4; i++ {
		octet := octets[0][i]
		n, err := strconv.Atoi(octet)
		if err != nil {
			return "", false
		}
		if n < 0 || n > 255 {
			return "", false
		}
	}
	// Validation checks passed
	return strings.Join(octets[0], "."), true
}

func parseQuery(m *dns.Msg) {
	for _, question := range m.Question {
		switch question.Qtype {
		// Only respond to A record requests
		case dns.TypeA:
			// If we match the proper format
			if ec2internalFormat.MatchString(question.Name) {
				ip, valid := extractIp(question.Name)
				if valid {
					rr, err := dns.NewRR(fmt.Sprintf("%s A %s", question.Name, ip))
					if err == nil {
						m.Answer = append(m.Answer, rr)
					}
				}
			}
		}
	}
}

func dnsResponder(w dns.ResponseWriter, r *dns.Msg) {
	rsp := new(dns.Msg)
	rsp.SetReply(r)
	rsp.Compress = false

	switch r.Opcode {
	case dns.OpcodeQuery:
		parseQuery(rsp)
	}

	err := w.WriteMsg(rsp)
	if err != nil {
		log.Print(err)
	}
}

func main() {
	log.Print("ec2internaldns responds to DNS queries on udp/53 only")
	log.Print("Queries in the form ip-a-b-c-d.ec2.internal. will be responded to with an IP of a.b.c.d")
	log.Print("Queries in the form ip-a-b-c-d.compute.internal. will be responded to with an IP of a.b.c.d")

	var err error
	ec2internalFormat, err = regexp.Compile("ip-(\\d{1,3})-(\\d{1,3})-(\\d{1,3})-(\\d{1,3})\\.(?:ec2|compute)\\.internal")
	if err != nil {
		log.Fatal(err)
	}

	dns.HandleFunc("ec2.internal.", dnsResponder)
	dns.HandleFunc("compute.internal.", dnsResponder)
	server := &dns.Server{Addr: ":53", Net: "udp4"}
	err = server.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	}
}
