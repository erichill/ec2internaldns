module gitlab.com/erichill/ec2internaldns

go 1.12

require (
	github.com/miekg/dns v1.1.15
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80 // indirect
	golang.org/x/sys v0.0.0-20190804053845-51ab0e2deafa // indirect
)
